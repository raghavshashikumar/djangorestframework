from django.conf.urls import url, include
from rest_framework import routers
from . import views

app_name = 'Bank'
urlpatterns = [
    url('bank/', views.BankListView.as_view(),name='list'),
    url('bank/<int:pk>/', views.BankDetailView.as_view(),name='detail'),
    url('branch/', views.BranchListView.as_view(),name='list'),
    url('branch/<int:pk>/', views.BranchDetailView.as_view(),name='detail'),
]
